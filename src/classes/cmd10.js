class Comando10 {
  constructor(position, type, statePhaseHex, idMeterHex) {
    this.position = position;
    this.type = type;
    this.statePhase = statePhaseHex;
    this.idMeter = idMeterHex;
  }

  getPosition() {
    return (parseInt(this.position, 16));
  }

  getType() {
    return (parseInt(this.type, 16));
  }

  getStatePhase() {
    return (parseInt(this.statePhase, 16));
  }

  getIdMeter() {
    let id = '';
    for (let i = 0; i < this.idMeter.length; i += 2) {
      id += String.fromCharCode(parseInt(this.idMeter.slice(i, (i + 2)), 16));
    }
    return id;
  }
}

module.exports = Comando10;
