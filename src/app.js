const express = require('express');
const morgan = require('morgan');
require('dotenv').config();
const helmet = require('helmet');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(helmet());

app.use(morgan('tiny'));

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`Escuchando en:${port}`);
});

app.use('/', require('./routes/index'));
