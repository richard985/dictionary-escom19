class Comando20 {
  constructor(posHex, voltajehex, corrienteHex, meteringReadingHex) {
    this.pos = posHex;
    this.voltaje = voltajehex;
    this.corriente = corrienteHex;
    this.meteringReading = meteringReadingHex;
  }

  getPosition() {
    return (parseInt(this.pos, 16));
  }

  getVoltaje() {
    return (parseInt(this.voltaje, 16) / 100);
  }

  getCorriente() {
    return (parseInt(this.corriente, 16) / 100);
  }

  getMeteringReading() {
    return (parseInt(this.meteringReading, 16));
  }
}

module.exports = Comando20;
