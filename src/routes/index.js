const express = require('express');
const Comando20 = require('../classes/cmd20');
const Comando10 = require('../classes/cmd10');

const router = express.Router();

router.post('/decode/cmd20', async (req, res) => {
  try {
    // console.log(req.body.id);
    const { last_data } = req.body.data;
    const posHex = last_data.slice(4, 6);
    const voltajeHex = last_data.slice(6, 10);
    const corrienteHex = last_data.slice(10, 14);
    const meteringReadingHex = last_data.slice(14, 22);
    const cmd20 = new Comando20(posHex, voltajeHex, corrienteHex, meteringReadingHex);
    const position = cmd20.getPosition();
    const voltaje = cmd20.getVoltaje();
    const corriente = cmd20.getCorriente();
    const meteringReading = cmd20.getMeteringReading();

    res.send({
      position, voltaje, corriente, meteringReading,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.post('/decode/cmd10', async (req, res) => {
  try {
    // console.log(req.body.id);
    const { last_data } = req.body.data;
    const posHex = last_data.slice(4, 6);
    const typeHex = last_data.slice(6, 8);
    const statePhaseHex = last_data.slice(8, 10);
    const idMeterHex = last_data.slice(10, 22);
    const cmd10 = new Comando10(posHex, typeHex, statePhaseHex, idMeterHex);
    const position = cmd10.getPosition();
    const type = cmd10.getType();
    const statePhase = cmd10.getStatePhase();
    const idMeter = cmd10.getIdMeter();
    res.send({
      position, type, statePhase, idMeter,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send(error);
  }
});

router.get('/health', (req, res) => res.status(200).send('🍔🍔🍔🍔🍔'));

module.exports = router;
