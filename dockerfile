FROM node:14-alpine 

WORKDIR  /src

COPY package*.json ./

RUN yarn install 

#Copiar todo los archivos dentro del directorio igual
COPY . . 
RUN apk add curl
HEALTHCHECK --interval=40s CMD curl -f http://localhost:4547/health || exit 1

 
#Ejecuto el proyecto
 CMD [ "yarn","start" ]